package com.example.p4papp;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.os.Build;

public class MainActivity extends Activity {
	
	private WifiManager wifi;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
		
		wifiScan();
	}
	
public void wifiScan(){
    	
    	wifi = (WifiManager)getSystemService(Context.WIFI_SERVICE);
        if(wifi.isWifiEnabled()){
        wifi.startScan();
        registerReceiver(new BroadcastReceiver(){
			@Override
			public void onReceive(Context context, Intent intent) {
				TextView t = (TextView)findViewById(R.id.textView1);
				t.setText("");
				t.setVisibility(t.GONE);
				
				for(ScanResult result : wifi.getScanResults()){
        			String signalStrenght = "" + result.BSSID + " : " + round(calculateDistance(result.level, result.frequency), 2)+"m |" + result.SSID  + "|\n";
        			String temp = (String) t.getText();
        			temp += signalStrenght;
        			t.setText(temp);
        		}
				t.setVisibility(t.VISIBLE);
				ProgressBar p = (ProgressBar)findViewById(R.id.progressBar1);
		        p.setVisibility(p.GONE);
			}
        }, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        }
        else{
        	TextView t = (TextView)findViewById(R.id.textView1);
        	
        	t.setText("Zet wifi aan!");
        }
    }
    
    public void refreshClick(View v){
    	wifiScan();
    }
    
    public String getClosest(List<ScanResult> wifiresults){
    	double min = Double.MAX_VALUE;
    	String minstring = "";
    	for (ScanResult result : wifiresults) {
			if(calculateDistance(result.level, result.frequency) <= min){
				min = calculateDistance(result.level, result.frequency);
				minstring = result.BSSID + "|" + result.SSID;
			}
		}
    	
    	return minstring + " : " + min;
    }
    
    public void buttonClick(View v) {
		
		TextView t = (TextView)findViewById(R.id.textView2);
		t.setText(getClosest(wifi.getScanResults()));
	
    }
    
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
    
    public double calculateDistance(double level, double frequency){
    	double exp = (27.55 - (20*Math.log10(frequency)) + Math.abs(level)) / 20.0;
    	return Math.pow(10.0, exp);
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			return rootView;
		}
	}

}
